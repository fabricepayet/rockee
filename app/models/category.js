import mongoose from 'mongoose'
import SlugCounter from './slugCounter'

let categorySchema = new mongoose.Schema({
  created: {
    type: Date,
    default: Date.now
  },
  modified: {
    type: Date,
    default: Date.now
  },
  slug: {
    type: String,
    unique: true,
    require: true
  },
  title: {
    type: String,
    require: true
  }
})

categorySchema.pre('save', function (next) {
  let { collectionName } = this.constructor.collection

  SlugCounter.slugify(this.title, collectionName, (err, slug) => {
    if (err) console.error(err)
    else this.slug = slug

    return next()
  })
})

export default mongoose.model('Category', categorySchema)
