import mongoose from 'mongoose'
import { shuffle } from 'underscore'

let questionSchema = new mongoose.Schema({
  created: {
    type: Date,
    default: Date.now
  },
  modified: {
    type: Date,
    default: Date.now
  },
  categorySlug: {
    type: String,
    require: true
  },
  source: {
    type: String,
    require: true
  },
  topic: {
    type: String
  },
  question: {
    type: String,
    require: true
  }
})

questionSchema.statics.findRandom = function (conditions, limit, cb) {
  return this.find(conditions, function (err, questions) {
    if (err) {
      return cb(err)
    }

    questions = shuffle(questions).slice(0, limit)
    cb(err, questions)
  })
}

export default mongoose.model('Question', questionSchema)
