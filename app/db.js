import _ from 'underscore'
import mongoose from 'mongoose'

const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017/rockee'

export default {
  get connection () { return mongoose.connection },
  connect (callback = _.noop) { return mongoose.connect(MONGO_URL, callback) },
  close () { return mongoose.connection.close() }
}
