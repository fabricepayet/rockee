require('babel-core/register')

const async = require('async')
const db = require('../db')
const Category = require('../models/category')
const Question = require('../models/question')
const categories = require('./categories')

async.series([
  db.connect,

  function (nextTask) {
    db.connection.db.dropDatabase(nextTask)
  },

  function (nextTask) {
    importCategories(categories, nextTask)
  }
], function (err) {
  db.close()

  if (err) return console.error(err)
  console.log('--> import done!')
})

function importCategories (categories, doneCallback) {
  async.eachSeries(
    categories,

    function (category, nextCategoryCallback) {
      async.series([
        createCategory.bind(null, category),
        importQuestions.bind(null, category.questions)
      ], nextCategoryCallback)
    },

    doneCallback
  )
}

function createCategory (category, doneCallback) {
  new Category({ title: category.title }).save(function (err, doc) {
    if (err) return console.error(err)

    category.questions = category.questions.map(function (question) {
      question.categorySlug = doc.slug
      return question
    })

    doneCallback()
  })
}

function importQuestions (questions, doneCallback) {
  async.eachSeries(
    questions,
    createQuestion,
    doneCallback
  )
}

function createQuestion (question, doneCallback) {
  new Question(question).save(doneCallback)
}
