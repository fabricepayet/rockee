import categoryApi from './category.api'

export default function (app) {
  categoryApi(app)

  app.get('/', function (req, res) {
    res.render('index')
  })
}
