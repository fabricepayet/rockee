import React from 'react'
import './AdaptableForm.less'

import MessageAction from '../../actions/MessageAction'
import MessageStore from '../../stores/MessageStore'

const ENTER_KEY = 13

let AdaptableForm = React.createClass({
  sendAnswer (event) {
    if (event.type === 'submit') event.preventDefault()

    if (MessageStore.isExpectingAnswer() !== true) return
    if (event.type === 'keydown' && event.keyCode !== ENTER_KEY) return

    let input = this.refs.input.getDOMNode()
    let message = input.value.trim()
    if (message.length === 0) return

    event.preventDefault()

    MessageAction.sendMessage(message)
    input.value = ''
  },

  render () {
    return (
      <div className='adaptable-form'>
        <form className='adaptable-form__body flex-row' onSubmit={this.sendAnswer}>
          <div className='flex-row__item flex-row__item--fill'>
            <textarea className='form-input'
                      ref='input'
                      autoFocus
                      onKeyDown={this.sendAnswer}
                      placeholder='Send a message...'>
            </textarea>
          </div>

          <button type='submit' className='flex-row__item button'>
            <span className='icon-send'></span>
          </button>
        </form>

        <div className='adaptable-form__footer'>
          <ul className='list-inline'>
            <li><a href='#'>Oops, dunno the answer</a></li>
          </ul>
        </div>
      </div>
    )
  }
})

export default AdaptableForm
