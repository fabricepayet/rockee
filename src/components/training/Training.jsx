import React from 'react'

import MessageAction from '../../actions/MessageAction'
import MessageStore from '../../stores/MessageStore'

import Messages from '../messages/Messages.jsx'
import AdaptableForm from '../adaptableForm/AdaptableForm.jsx'

let Training = React.createClass({
  propTypes: {
    routeParams: React.PropTypes.object
  },

  getInitialState () {
    return MessageStore.getAll()
  },

  onChange () {
    this.setState(MessageStore.getAll())
  },

  componentDidMount () {
    MessageStore.addChangeListener(this.onChange)
    MessageAction.newInterview(this.props.routeParams.slug)
  },

  componentWillUnmount () {
    MessageStore.removeChangeListener(this.onChange)
  },

  componentDidUpdate () {
    let scrollableContainer = document.getElementById('scrollable-container')

    if (scrollableContainer) {
      scrollableContainer.scrollTop = scrollableContainer.scrollHeight
    }
  },

  render () {
    return (
      <main className='main-column'>
        <div className='main-column__header'>
          <h1>
            <span className='icon-training'></span>
            &nbsp;Training
          </h1>
        </div>

        <div id='scrollable-container' className='main-column__content'>
          <ul className='thread'>
            {this.state.messages.map(function (msg, index) {
              return <Messages messages={msg.messages} author={msg.author} isBot={msg.isBot} key={index} />
            })}
          </ul>

          <small>{this.state.isTyping && 'Rockee is typing...'}&nbsp;</small>
        </div>

        <AdaptableForm />
      </main>
    )
  }
})

export default Training
