import React from 'react'
import './Main.less'
import LeftColumn from '../leftColumn/LeftColumn.jsx'

let Main = React.createClass({
  propTypes: {
    children: React.PropTypes.object
  },

  render: function () {
    return (
      <div className='container'>
        <LeftColumn />
        {this.props.children}
      </div>
    )
  }
})

export default Main
